class CreateListings < ActiveRecord::Migration[5.0]
  def change
    create_table :listings do |t|
      t.string :name
      t.text :dates_blocked

      t.timestamps
    end
  end
end
