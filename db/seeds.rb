listing_1 = Listing.create(
  name: 'Listing #1',
  vehicle_type: 'Class A',
  dates_blocked: ['2017-04-05', '2017-04-06', '2017-04-07']
)
listing_2 = Listing.create(
  name: 'Listing #2',
  vehicle_type: 'Class B',
  dates_blocked: ['2017-04-09', '2017-04-10']
)
listing_3 = Listing.create(
  name: 'Listing #3',
  vehicle_type: 'Fifth Wheel',
  dates_blocked: ['2017-04-03', '2017-04-04', '2017-04-05']
)

Reservation.create(listing: listing_1, start_date: '2017-04-01', end_date: '2017-04-04', status: 'PENDING')
Reservation.create(listing: listing_1, start_date: '2017-04-09', end_date: '2017-04-12', status: 'CONFIRMED')
Reservation.create(listing: listing_2, start_date: '2017-04-03', end_date: '2017-04-06', status: 'PENDING')
Reservation.create(listing: listing_2, start_date: '2017-04-13', end_date: '2017-04-20', status: 'CONFIRMED')
Reservation.create(listing: listing_3, start_date: '2017-04-01', end_date: '2017-04-02', status: 'PENDING')
Reservation.create(listing: listing_3, start_date: '2017-04-06', end_date: '2017-04-21', status: 'CONFIRMED')