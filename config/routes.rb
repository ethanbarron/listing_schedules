Rails.application.routes.draw do
  root to: 'timeline#index'

  get '/data.json', to: 'timeline#data'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
