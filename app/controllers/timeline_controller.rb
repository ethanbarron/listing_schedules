class TimelineController < ApplicationController
  def index
  end

  def data
    render json: {
      sections: build_sections_data,
      items: build_items_data
    }
  end

  private

  def build_sections_data
    #[
    #  {
    #    id: 1,
    #    name: 'Section 1'
    #  }
    #]
    sections = []
    current_user_listings.each do |listing|
      sections << {
        id: listing.id,
        name: listing.name
      }
    end
    sections
  end

  def build_items_data
    #[
    #  {
    #      id: 10,
    #      name: '<div>Item 1</div><div>Sub Info</div>',
    #      sectionID: 1,
    #      start: Date.today.beginning_of_day,
    #      end: Date.today.end_of_day,
    #      classes: 'item-status-none'
    #  },
    #  {
    #      id: 11,
    #      name: '<div>Item 2</div><div>Sub Info</div>',
    #      sectionID: 1,
    #      start: (Date.today + 1.day).beginning_of_day,
    #      end: (Date.today + 1.day).end_of_day,
    #      classes: 'item-status-none'
    #  }
    #]
    items = []
    current_user_listings.each do |listing|
      listing_reservations(listing).each do |reservation|
        items << {
          id: reservation.id,
          sectionID: listing.id,
          name: '<div>&nbsp;</div><div>&nbsp;</div>',
          start: reservation.start_date.beginning_of_day,
          end: reservation.end_date.end_of_day,
          classes: 'item-status-none'
        }
      end
      listing_dates_blocked(listing).each do |range|
        items << {
          id: 12345,
          sectionID: listing.id,
          name: '<div>&nbsp;</div><div>&nbsp;</div>',
          start: range.first.beginning_of_day,
          end: range.last.end_of_day,
          classes: 'item-status-one'
        }
      end
    end
    items
  end

  def listing_dates_blocked(listing)
    # get blocked date ranges
    dates = listing.dates_blocked.map(&:to_date)
    prev = dates.first
    dates.slice_before do |el|
      prev, prev2 = el, prev
      prev2 + 1.day != el
    end
  end

  def current_user_listings
    query = Listing
    query = query.where(vehicle_type: params[:vehicle_type]) unless
      params[:vehicle_type] == 'any' || params[:vehicle_type].nil?
    query.all
  end

  def listing_reservations(listing)
    query = listing.reservations
    query = query.where(status: params[:status]) unless
      params[:status] == 'any' || params[:status].nil?
    query.all
  end
end
