class Listing < ApplicationRecord
  serialize :dates_blocked, Array
  
  has_many :reservations
end
