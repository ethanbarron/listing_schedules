﻿var today = moment().startOf('day');

var Calendar = {
    Periods: [
        {
            Name: '3 days',
            Label: '3 days',
            TimeframePeriod: (60 * 3),
            TimeframeOverall: (60 * 24 * 3),
            TimeframeHeaders: [
                'Do MMM',
                'HH'
            ]
        },
        {
            Name: '1 week',
            Label: '1 week',
            TimeframePeriod: (60 * 24),
            TimeframeOverall: (60 * 24 * 7),
            TimeframeHeaders: [
                'MMM',
                'Do'
            ]
        },
        {
            Name: '1 month',
            Label: '1 month',
            TimeframePeriod: (60 * 24),
            TimeframeOverall: (60 * 24 * 28),
            TimeframeHeaders: [
                'MMM',
                'Do'
            ]
        }
    ],

    Init: function () {
        TimeScheduler.Options.GetSections = Calendar.GetSections;
        TimeScheduler.Options.GetSchedule = Calendar.GetSchedule;
        TimeScheduler.Options.Start = today;
        TimeScheduler.Options.Periods = Calendar.Periods;
        TimeScheduler.Options.SelectedPeriod = '1 week';
        TimeScheduler.Options.Element = $('.calendar');

        TimeScheduler.Options.AllowDragging = false;
        TimeScheduler.Options.AllowResizing = false;

        TimeScheduler.Options.Events.ItemClicked = Calendar.Item_Clicked;

        TimeScheduler.Options.Text.NextButton = '&nbsp;';
        TimeScheduler.Options.Text.PrevButton = '&nbsp;';

        TimeScheduler.Options.MaxHeight = 100;

        TimeScheduler.Init();
    },

    GetSections: function (callback) {
        callback(Calendar.Sections);
    },

    GetSchedule: function (callback, start, end) {
        callback(Calendar.Items);
    },

    Item_Clicked: function (item) {
        console.log(item);
    }
};

function fillTimeline(data) {
    Calendar.Sections = data['sections'];

    var items = [];
    $.each(data['items'], function(arrIndex, arrValue) {
        var item = arrValue;
        item['start'] = moment(item['start']);
        item['end'] = moment(item['end']);
        console.log(item);
        items.push(item);
    });
    Calendar.Items = items;

    TimeScheduler.FillSections(true);
    Calendar.Init();
}

function getTimelineData(callback) {
    var dataUrl = '/data.json?';
    $.each($('.timeline-filter'), function(idx, filter) {
        var filterId = $(filter).attr('id');
        var filterVal = $(filter).val();

        if(idx > 0)
            dataUrl = dataUrl + '&';
        dataUrl = dataUrl + filterId + '=' + filterVal;
    });
    console.log('Fetching timeline data from url:', dataUrl);
    $.ajax(dataUrl, {
        success: function(data) {
            callback(data);
        },
        error: function(error) {
            console.log("ERROR:", error);
        }
    })
}

$(document).on('change', '.timeline-filter', function() {
    var filterId = $(this).attr('id');
    console.log(filterId + ' changed! Refilling timeline...');
    getTimelineData(fillTimeline);
});

$(document).ready(function() {
    getTimelineData(fillTimeline);
});